﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{

    [HideInInspector] public bool facingRight = true;  //sets the character facing right at the start of the game 
    [HideInInspector] public bool jump = false;   //sets jump to false at the beginning of the game
    [HideInInspector] public bool IsAttacking = false;  //sets the attacking animation to false at the beginning of the game

    //sets the main characters movement varaiables(how main character controls in world space)
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform GroundCheck;
    public bool attacking = false;
    private GameMaster gm;

    AudioSource aS;
    public AudioClip collectSFX;
    public AudioClip[] collect2SFX;
    

    public bool grounded = false;
    public Animator anim;
    private Rigidbody2D rb2d;


    private SpriteRenderer spriteRenderer;
    private Sprite openSprite, closedSprite;
    public bool isOpen;

    void Start()
    {
        anim.GetComponent<Animator>();  //grabs the chracter's animator
        gm = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        aS = GetComponent<AudioSource>(); //gets the audio surce component 
    }

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
      
    }
   

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.Linecast(transform.position, GroundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (Input.GetButtonDown("Jump") && grounded) //checks if the player is jumping and is grounded 
        {
            jump = true;
        }

        if (Input.GetKeyDown("f")) //checks if the player has hit the f key
        {
            anim.Play("Attack");
        }

        if (Input.GetKeyUp("f")) //checks to see if the player has let go of the f key 
        {
            anim.SetTrigger("Idle");
        }
        
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && facingRight)
            Flip();
        else if (h < 0 && !facingRight)
            Flip();

        if (jump)
        {
            
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false; //makes sure the playr can't jump a second time in the air
        }

    }


    void Flip()  //sets the character orientation 
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Coin")) //checks if the player collides with a coin
        {
            Destroy(collision.gameObject); //destroy's coin on collision
            gm.points += 1;  //adds one coins to the player's coin counter
            aS.clip = collectSFX; //stores the coin sound effect 
            aS.Play();  //plays coin sound effect 

            //Destroy coin when player collides with it 
        }

        if (collision.gameObject.CompareTag("Death"))  //checks if the player collides with the death box
        {
          
            Destroy(collision.gameObject); //destroys the object on collision with the death box at the bottom of the level
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }

        if (collision.gameObject.CompareTag("Enemy")) //checks if the player collides with the enemys
        {
        
            Destroy(collision.gameObject);  //destroys the player if he collides with the enemy 
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            
        }
        
    }
    
}