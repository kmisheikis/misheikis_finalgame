﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class YouWin : MonoBehaviour
{
    public float restartDelay = 20f; //creates a delay before restarting the game 
    public Animator anim; //grabs the chests animator 
    

    void Awake()
    {

    
    }
   
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) //checks if the player has collided with the chest at the end of the game 
        {
            StartCoroutine(Test());

            anim.gameObject.SetActive(false); //sets the objects animation to false 
            

            anim.gameObject.SetActive(true);
            anim.SetTrigger("YouWin"); //sets the trigger for the animation to true so the animation plays 

           
        }

        
    }
    IEnumerator Test()
    {
        yield return new WaitForSeconds(10); //makes the game wwait for ten seconds before restarting 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1); //loads the main menu screen after ten seconds 
    }
}
