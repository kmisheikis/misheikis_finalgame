﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        //Starts the game when the start button is pressed 
    }
    public void QuitGame()
    {
        Application.Quit();
        //Quits the game whent he quit button is pressed 
    }
    
}
