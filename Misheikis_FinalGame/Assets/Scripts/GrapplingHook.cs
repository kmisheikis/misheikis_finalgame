﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    DistanceJoint2D joint; //sets up the distance joint 
    Vector3 targetPos;  //give the distance joint a target position to connect to
    RaycastHit2D hit;   //checks whether the raycast hit the intented target
    public float distance = 10f; //sets the max distance the player can shoot the grapple
    public LayerMask mask;  
    public LineRenderer line;  //renders the line
    public float step = 0.2f;
    bool grappled = false;    //sets whether or not the grapple is grappling
    // Start is called before the first frame update
    void Start()
    {
        joint = GetComponent<DistanceJoint2D>();  //creates the distance joint at the start of the game 
        joint.enabled = false;  //joint starts turned off so that the player can't see it at all times just when they hit the grapple button
        line.enabled = false;  //line starts off as false
    }

    // Update is called once per frame
    void Update()
    {
        
        if (joint.distance > 1f)
        {
            joint.distance -= step;
        }
        else
        {
            line.enabled = false;
            joint.enabled = false;
        }

        if (!grappled)
        {
            //when not grappled
            if (Input.GetMouseButton(0))
            {
                
                targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //gets the position of the mous on the screen to set a grapple point
                targetPos.z = 0;

                hit = Physics2D.Raycast(transform.position, targetPos - transform.position, distance, mask);
                if (hit.collider != null)  //checks if the grapple hit the intended target
                {
                    grappled = true;
                }
            }
        }
        else
        {
            //when grappled
            if (hit.collider != null && hit.collider.gameObject.GetComponent<Rigidbody2D>() != null)
            {
                print("has hit!");
                joint.enabled = true;
                joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody2D>();
                joint.connectedAnchor = hit.point - new Vector2(hit.collider.transform.position.x, hit.collider.transform.position.y);
                joint.distance = Vector2.Distance(transform.position, hit.point);
                line.enabled = true;
                line.SetPosition(0, transform.position);
                line.SetPosition(1, hit.point);

                if (!Input.GetMouseButton(0))
                {
                    print("turn off!");
                    joint.enabled = false;
                    grappled = false;
                    line.enabled = false;
                }

                if (Input.GetMouseButton(0))  //checks if the mouse button is hit
                {
                    line.SetPosition(0, transform.position);
                }

            }
            else { grappled = false; }
        }

      

        
        
    }
}
