﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    public int points;
    public Text CoinText;
    public Text CoinText2; 

  
    // Update is called once per frame
    void Update()
    {
        CoinText.text = ("Coins: " + points + "/124"); //sets the in-game text to show the player how many coins they have left to grab
        CoinText2.text = ("Coins: " + points + "/124"); //sets the end game text to show the player how many coins they collected over the game 

        
    }
}
